# TelegramSphereBot

A tiny Telegram Bot (polling-based) written for the Azure Sphere board.

## How to run

To run code on the Azure Sphere you need a **recent enough version of Visual Studio** (I used 2019 Community), and the **Sphere SDK** installed.

After importing the project, create a file called `apikey.h`, and put inside it the following:

```c
// Telegram Bot API endpoints and stuff
#define APIKEY "YOURAPIKEY/" // your bot API key goes here, remember NO start slash, only one at the end!
```

then and press the `Remote GDB Debugger` button in the Visual Studio toolbar.

## Available commands

 | **Command** 	| **Description**                 	|
|-------------	|---------------------------------	|
|  `/on`      	| Turn on a User LED on the board 	|
| `/off`      	| Turn on a User LED on the board 	|
| `/ayy`      	| Guess what?                     	|

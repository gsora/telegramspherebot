﻿#include "main.h"


int main(void) {
	if (initI2c() == -1) {
		return -1;
	}

	Log_Debug("%s", GETUPDATES_ENDPOINT);

	gpio = GPIO_OpenAsOutput(9, GPIO_OutputMode_PushPull, GPIO_Value_High);
	if (gpio < 0) {
		Log_Debug(
			"Error opening GPIO: %s (%d). Check that app_manifest.json includes the GPIO used.\n",
			strerror(errno), errno);
		return -1;
	}

	certs_path = Storage_GetAbsolutePathInImagePackage("certs/api_telegram_org.crt");
	if (certs_path == NULL) {
		Log_Debug("The certificate path could not be resolved: errno=%d (%s)\n", errno,
			strerror(errno));
		return -1;
	}

	CURLcode res = curl_global_init(CURL_GLOBAL_ALL);

	polling_c = curl_easy_init();
	if (!polling_c) {
		Log_Debug("could not initialize curl! %s %d", strerror(errno), errno);
		return -1;
	}

	if ((resinit = curl_easy_setopt(polling_c, CURLOPT_CAINFO, certs_path)) != CURLE_OK) {
		Log_Debug("could not initialize curl! %s %d", strerror(errno), errno);
		return -1;
	}

	send_message_c = curl_easy_init();
	if (!send_message_c) {
		Log_Debug("could not initialize curl! %s %d", strerror(errno), errno);
		return -1;
	}

	if ((resinit = curl_easy_setopt(send_message_c, CURLOPT_CAINFO, certs_path)) != CURLE_OK) {
		Log_Debug("could not initialize curl! %s %d", strerror(errno), errno);
		return -1;
	}

	if (DEBUG) {
		curl_easy_setopt(polling_c, CURLOPT_VERBOSE, 1L);
	}
	const struct timespec sleepTime = { 1, 0 };
	while (true) {
		char* url = new_update_url();

		curl_easy_setopt(polling_c, CURLOPT_URL, url);
		curl_easy_setopt(polling_c, CURLOPT_WRITEFUNCTION, new_updates_callback);
		res = curl_easy_perform(polling_c);
		if (res != CURLE_OK)
			Log_Debug("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

		free(url);
		nanosleep(&sleepTime, NULL);
	}

	curl_global_cleanup();
}

size_t new_updates_callback(char* ptr, size_t size, size_t nmemb, void* userdata) {
	bool ok = false;

	cJSON* json = cJSON_Parse(ptr);
	if (json == NULL) {
		const char* error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL) {
			Log_Debug("Error before: %s\n", error_ptr);
		}

		// let's increment update_offset to ignore this update, shall we...
		update_offset++;
		return (size_t)-1;
	}

	cJSON* raw_ok = cJSON_GetObjectItem(json, "ok");
	if (cJSON_IsBool(raw_ok)) {
		ok = raw_ok->valueint;
	}

	if (!ok) {
		return (size_t)-1;
	}

	int user_destination = 0;
	char* text;

	cJSON* updates = cJSON_GetObjectItemCaseSensitive(json, "result");
	cJSON* update;
	cJSON_ArrayForEach(update, updates) {
		// since we used limit 1, we already know we only have one update,
		// we use this function just to assing values to update
		Log_Debug("Got some updates from telegram!\n");

		cJSON* update_id = cJSON_GetObjectItem(update, "update_id");
		if (!cJSON_IsNumber(update_id)) {
			Log_Debug("Could not retrieve update_id, cannot continue\n");
			return (size_t)-1;
		}

		update_offset = update_id->valueint + 1;

		cJSON* message = cJSON_GetObjectItem(update, "message");
		cJSON* from = cJSON_GetObjectItem(message, "from");

		cJSON* id = cJSON_GetObjectItem(from, "id");
		if (!cJSON_IsNumber(id)) {
			Log_Debug("Could not retrieve destination id, cannot continue\n");
			return (size_t)-1;
		}

		user_destination = id->valueint;
		Log_Debug("Got user id: %d\n", user_destination);

		cJSON* raw_text = cJSON_GetObjectItem(message, "text");
		text = cJSON_GetStringValue(raw_text);
		Log_Debug("Got user message: \"%s\"\n", text);

		parse_command(text, user_destination);
	}

	cJSON_Delete(json);

	return (size_t)strlen(ptr);
}

size_t message_sent_callback(char* ptr, size_t size, size_t nmemb, void* userdata) {
	bool ok = false;

	cJSON* json = cJSON_Parse(ptr);
	if (json == NULL) {
		const char* error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL) {
			Log_Debug("Error before: %s\n", error_ptr);
			return (size_t)-1;
		}

		// let's increment update_offset to ignore this update, shall we...
		update_offset++;
		return (size_t)-1;
	}

	cJSON* raw_ok = cJSON_GetObjectItem(json, "ok");
	if (cJSON_IsBool(raw_ok)) {
		ok = raw_ok->valueint;
	}

	if (!ok) {
		Log_Debug("telegram reported some kind of error when deploying a message to an user, sad\n");
		return (size_t)-1;
	}

	return (size_t)strlen(ptr);
}

void parse_command(char* msg, int dest) {
	if (strcmp("/on", msg) == 0) {
		GPIO_SetValue(gpio, GPIO_Value_Low);
		return;
	}

	if (strcmp("/off", msg) == 0) {
		GPIO_SetValue(gpio, GPIO_Value_High);
		return;
	}
	
	if (strcmp("/ayy", msg) == 0) {
		int res = send_message("lmao", dest);
		if (res != 0) {
			Log_Debug("could not send message! %s %s", strerror(errno), curl_easy_strerror(res));
		}
		return;
	}
	if (strcmp("/temperature", msg) == 0) {
		lps22hh_temp *t = temp();
		char t_string[128];
		sprintf(t_string, "Temperature: %d °C\nPressure: %d hPa\n", t->temp, t->pressure);
		int res = send_message(t_string, dest);
		if (res != 0) {
			Log_Debug("could not send message! %s %s", strerror(errno), curl_easy_strerror(res));
		}
		free(t);
		return;
	}
}

char* new_update_url() {
	char* update_offset_str = convert_int_to_string(update_offset);
	char* r = malloc(strlen(GETUPDATES_ENDPOINT) + strlen(update_offset_str) * sizeof(char));
	sprintf(r, GETUPDATES_ENDPOINT, update_offset_str);
	free(update_offset_str);
	return r;
}

char* new_message_url(char* message, int destination, CURL *handle) {
	char* dest_str = convert_int_to_string(destination);
	char* data = curl_easy_escape(handle, message, 0);
	if (data == NULL) {
		return NULL;
	}
	char* url = malloc(strlen(SEND_MESSAGE_ENDPOINT) + strlen(dest_str) + strlen(data) * sizeof(char));
	sprintf(url, SEND_MESSAGE_ENDPOINT, data, dest_str);
	free(dest_str);
	free(data);

	return url;
}

char* convert_int_to_string(int value) {
	unsigned int digitcount;
	char* tmp_string;
	unsigned int increment = 2; // one for rounding, one for '\0' terminator
	if (value < 0) {
		increment += 1; // make room for sign 
	}
	if (0 == value) {
		tmp_string = malloc(2 * sizeof(char));
		sprintf(tmp_string, "%u", 0);
	}
	else {
		digitcount = (unsigned int)floor(log10((double)abs(value))) + increment;
		tmp_string = malloc(digitcount * sizeof(char));
		sprintf(tmp_string, "%d", value);
	}
	return tmp_string;
}

int send_message(char* message, int destination) {
	CURLcode res;

	char* url = new_message_url(message, destination, send_message_c);
	if (url == NULL) {
		Log_Debug("url was NULL!");
		return -1;
	}
	curl_easy_setopt(send_message_c, CURLOPT_URL, url);
	curl_easy_setopt(send_message_c, CURLOPT_WRITEFUNCTION, message_sent_callback);
	res = curl_easy_perform(send_message_c);
	free(url);

	return res;
}
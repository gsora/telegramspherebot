#pragma once

#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#include <applibs/log.h>
#include <applibs/gpio.h>
#include <applibs/storage.h>
#include "i2c.h"

#include <math.h>
#include <curl/curl.h>
#include "cJSON.h"
#include "apikey.h"

#define ENDPOINT_BASE "https://api.telegram.org/bot"
#define GETUPDATES_ENDPOINT ENDPOINT_BASE APIKEY "getUpdates?limit=1&offset=%s"
#define SEND_MESSAGE_ENDPOINT ENDPOINT_BASE APIKEY "sendMessage?text=%s&chat_id=%s"

// set to 1 to enable debug in curl
#define DEBUG 0

// Callback functions
// These functions gets called whenever CURL does a successful GET/POST request to Telegram 
// (depends on the kind of request).
size_t new_updates_callback(char* ptr, size_t size, size_t nmemb, void* userdata);
size_t message_sent_callback(char* ptr, size_t size, size_t nmemb, void* userdata);

// URL generation functions
// These functions return a free()-able pointer.
char* new_update_url(void);
char* new_message_url(char*, int, CURL*);

// parse_command parses a Telegram message, and replies to the chat if needed.
void parse_command(char*, int);

// Utility function to convert int to correctly-sized free()-able char*.
char* convert_int_to_string(int);

// send_message sends a Telegram message to destination.
int send_message(char*, int);

int update_offset = 0;
int gpio;
char* certs_path;
CURL* polling_c;
CURL* send_message_c;
CURLcode resinit;

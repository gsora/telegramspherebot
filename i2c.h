#pragma once

#include <stdbool.h>
#include "epoll_timerfd_utilities.h"

#define LSM6DSO_ID         0x6C   // register value
#define LSM6DSO_ADDRESS	   0x6A	  // I2C Address

typedef struct lps22hh_temp {
	int temp;
	int pressure;
}lps22hh_temp;

int initI2c(void);
void closeI2c(void);
lps22hh_temp* temp(void);